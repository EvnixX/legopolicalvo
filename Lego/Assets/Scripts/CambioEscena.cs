using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioEscena : MonoBehaviour
{
    public void Nivel1()
    {
        SceneManager.LoadScene("Nivel 1");
    }


    public void Salir()
    {
        Application.Quit();
    }

}
