using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Movement : MonoBehaviour
{
    public Animator anim;

    [Range(0, 10)]
    public float moveSpeed;
    public float rotationSpeed;

    Rigidbody rb;
    bool jumpFloor = false;
    bool jumpTrue = false;

    public float jumpForce = 5f;

    public AudioClip audioFX;

    private void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }


    private void Update()
    {
        Vector3 moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        anim.SetFloat("Speed", moveDirection.z);
        transform.Translate((moveDirection * moveSpeed) * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * rotationSpeed, 0));

        if(moveSpeed <= 5 && Input.GetButtonDown("Ctrl"))
        {
            moveSpeed = 8;
        }

        if (moveSpeed >= 5 && Input.GetButtonDown("Ctrl"))
        {
            moveSpeed = 5;
        }

        Vector3 floor = transform.TransformDirection(Vector3.down);

        if (Physics.Raycast(transform.position, floor, 1f))
        {
            jumpFloor = true;
        }
        else
        {
            jumpFloor = false;
        }
        jumpTrue = Input.GetButtonDown("Jump");

        if(jumpTrue && jumpFloor)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            AudioSource.PlayClipAtPoint(audioFX, gameObject.transform.position);
            anim.SetBool("Salto", true);
        }
        if (!jumpTrue && jumpFloor)
        {
            anim.SetBool("Salto", false);
        }
    }
}
